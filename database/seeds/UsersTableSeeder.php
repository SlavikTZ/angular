<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'first_name'=>'Вячеслав',
            'last_name'=>'Лысенко',
            'email'=>'slavik.tz@yandex.ru',
            'birth_day'=>'1981-01-31',
            'gander'=>'M',
            'address'=>'Зестафонская 1, кв. 5',
            'password'=>bcrypt('qwerty')
        ]);
        User::create([
            'first_name'=>'Сергей',
            'last_name'=>'Корж',
            'email'=>'sergey@yandex.ru',
            'birth_day'=>'1987-01-31',
            'gander'=>'M',
            'address'=>'Космическая 1, кв. 5',
            'password'=>bcrypt('qwerty')
        ]);
        User::create([
            'first_name'=>'Глеб',
            'last_name'=>'Моисеев',
            'email'=>'gleb@yandex.ru',
            'birth_day'=>'1987-01-31',
            'gander'=>'M',
            'address'=>'Космическая 1, кв. 5',
            'password'=>bcrypt('qwerty')
        ]);
    }
}
