<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{elixir('css/app.css')}}">
    <title>Angular</title>
</head>
<body ng-app="newApp">
@verbatim
    <div class="container main">
        <div class="row">
            <div class="col-sm-2">Bar</div>
            <div class="col-sm-10">
                  <user-card></user-card>
            </div>
        </div>
    </div>
@endverbatim


    <script src="{{elixir('js/angular.js')}}"></script>
    <script src="{{elixir('js/app.js')}}"></script>
</body>
</html>