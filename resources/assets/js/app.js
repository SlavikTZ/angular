/*

/!**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 *!/

require('./bootstrap');

window.Vue = require('vue');

/!**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 *!/

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});*/

app = angular.module('newApp',[]);
app.directive('userCard', function(){
    return {
        restrict: 'E',
        templateUrl:'js/components/user-card.html',
    };
});
app.controller('UserController', function(UserService){

    this.users = UserService.getAll();

    /*this.user={
     name:{label:"Имя", value:"Лысенко Вячеслав Петрович"},
     birth_date:{label:"Дата рождения", value:"31-01-1981"},
     gander:{label:"Пол", value:"М"},
     address:{label:"Адрес", value:"Зестафонская 1, кв. 5"},
     email:{label:"E-mail", value:"slavik.tz@yandex.ru"}
     };*/
});

app.service('UserService', function($http){
    var users;
    var self = this;

    $http({
        method: 'GET',
        url: 'http://angular/users'
    }).then(function successCallback(response) {
        users=response.data;
    }, function errorCallback(response) {
        console.log(response);
    });

    this.getOne = function(id){
        return this.users[id];
    }
    this.getAll = function(){
        console.log(this.users);
        return users;
    }

});

